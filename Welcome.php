<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('radio');//llamamos al modelo
	}
	public function index()
	{
		$data["listadoRadios"]=$listadoRadios = $this->radio->obtenerTodos();
		$this->load->view('welcome_message',$data);
	}

	// funcion para capturar los valores del formulario nuevo
	public function guardarRadio(){
		$datosNuevoRadio=array(
			// "id"=>$this->input->post('id'),
			"nombre"=>$this->input->post('nombre'),
			"latitud"=>$this->input->post('latitud'),
			"longitud"=>$this->input->post('longitud'),
			"rango"=>$this->input->post('rango'),
			"color"=>$this->input->post('color')
		);
		print_r($datosNuevoRadio);
		if ($this->radio->insertar($datosNuevoRadio)) {
			redirect ('');
		} else {
			echo "<h1>ERROR</h1>";
		}
	}
	//eliminar datos
	public function borrar($id){
		if ($this->radio->eliminarPorId($id)) {//radio es el nombre del modelo
			redirect ('');
		} else {
			echo "ERROR AL ELIMINAR :(";
		}
	}
}
