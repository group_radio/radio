<?php
  class Radio extends CI_Model
  {

    function __construct()
    {
      parent::__construct(); //invoca a la clase padre
    }
    public function insertar($datos){
      return $this->db->insert("radio",$datos);//insertar en la tabla radio
    }
    // funcion para consultar los datos de la tabla
    function obtenerTodos(){
      $rad=$this->db->get("radio");//radio es el nombre de la tabla en la base de datos rad es la variable
      if ($rad->num_rows()>0) {// si hay datos en la base de datos
        return $rad;
      } else {
        return false;// cuando no hay datos en la base de datos
      }

    }
    //eliminar
    public function eliminarPorId($id){
      $this->db->where("id",$id); // es el nombre del campo en la tabla radio
      return $this->db->delete("radio"); // radio el nombre de la tabla en la base de datos
    }
  }
